﻿using System.Threading.Tasks;
using EX4.Models;
using EX4.Services;
using Microsoft.AspNetCore.Mvc;

namespace EX4.Controllers
{
    public class ClassController : Controller
    {
        private readonly IClassService _classService;

        public ClassController(IClassService classService)
        {
            _classService = classService ;
        }

        //get getall
        public IActionResult Index()
        
        {
           
            return View(_classService.GetAll());
        }

      
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Class @class)
        {
            _classService.Insert(@class);
            return RedirectToAction("Index");
        }

//        [HttpGet]
//        public IActionResult Delete(int? Id)
//        {
//            return View(_studentServices.GetStudentByID(Id));
//        }
        [HttpGet]
        public IActionResult DeleteClass(int id)
        {
            _classService.Delete(id);
            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult Edit(int? Id)
        {
            return View(_classService.GetById(Id));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Class class1)
        {

            _classService.Update(class1);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult GetID(int? id)
        {
            _classService.GetById(id);
            return RedirectToAction("Index");
        }

    }
}