﻿using System.Threading.Tasks;
using EX4.Models;
using EX4.Services;
using Microsoft.AspNetCore.Mvc;

public class StudentController : Controller
{
    private readonly IStudentService _studentService;

    public StudentController(IStudentService studentService)
    {
        _studentService = studentService;
    }

    //get getall
    public IActionResult Index()
        
    {
           
        return View(_studentService.GetAll());
    }

    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Create(Student student)
    {
        _studentService.Insert(student);
        return RedirectToAction("Index");
    }

//        [HttpGet]
//        public IActionResult Delete(int? Id)
//        {
//            return View(_studentServices.GetStudentByID(Id));
//        }
    [HttpGet]
    public IActionResult Delete(int id)
    {
        _studentService.Delete(id);
        return RedirectToAction("Index");

    }

    [HttpGet]
    public IActionResult Edit(int? Id)
    {
        return View(_studentService.GetById(Id));
    }

    [HttpPost]
    public async Task<IActionResult> Edit(Student student)
    {

        _studentService.Update(student);
        return RedirectToAction("Index");
    }

    [HttpGet]
    public IActionResult GetID(int? id)
    {
        _studentService.GetById(id);
        return RedirectToAction("Index");
    }


}
