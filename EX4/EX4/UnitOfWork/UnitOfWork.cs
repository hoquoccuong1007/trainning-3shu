﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using EX4.Models;
using EX4.Repository;
using EX4.Services;

namespace EX4.UnitOfWork
{
    public  class UnitOfWork : IDisposable, IUnitOfWork
    {
        private StudentContext _context;
        private IMapper _mapper;

        public UnitOfWork(StudentContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            InitRepositories();
        }

        
        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed = false;

        public IGenericRepository<Class> classRepository { get; private set; }
        public IGenericRepository<Student> studentRepository { get; private set; }
        
        private void InitRepositories()
        {
            classRepository = new GenericRepository<Class>(_context,_mapper);
            studentRepository = new GenericRepository<Student>(_context,_mapper);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        
    }
}