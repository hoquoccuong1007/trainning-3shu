﻿using EX4.Models;
using EX4.Repository;
using EX4.Services;

namespace EX4.UnitOfWork
{
    public interface IUnitOfWork
    {
        IGenericRepository<Class> classRepository { get; }
        IGenericRepository<Student> studentRepository { get; }
        void Save();

    }

}