﻿using System;
using System.Collections.Generic;

namespace EX4.Services
{
    public interface IGenericRepository<TEntity>
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int? id);
        void Insert(TEntity  tentity);
        void Delete(int id);
        void Update(TEntity tentity);
        void save();
      
    }
}