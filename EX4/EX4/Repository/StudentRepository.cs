﻿using AutoMapper;
using EX4.Models;
using EX4.Services;

namespace EX4.Repository
{
    public class StudentRepository:GenericRepository<Student>,IStudentRepository
    {
        private IStudentRepository _studentRepository;

        public StudentRepository(StudentContext context, IMapper mapper) : base(context, mapper)
        {
        }


        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
    }
}