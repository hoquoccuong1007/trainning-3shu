﻿using AutoMapper;
using EX4.Models;

namespace EX4.Repository
{
    public class ClassRepository : GenericRepository<Class>, IClassRepository
    {
        public ClassRepository(StudentContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
    }
}