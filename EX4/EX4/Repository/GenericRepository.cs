﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EX4.Models;
using EX4.Services;
using EX4.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace EX4.Repository
{
    public class  GenericRepository<TEntity> :IGenericRepository<TEntity> where TEntity : class
    {
        
        
            

            private readonly StudentContext _context;
            private readonly IMapper _mapper;
            public GenericRepository(StudentContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

        

        public IEnumerable<TEntity> GetAll()
            {
                return _context.Set<TEntity>();
            }
            public TEntity GetById(int? id)
            {
                return _context.Set<TEntity>().Find(id);     
            }
            public void Insert(TEntity tentity)
            {
                _context.Set<TEntity>().Add(tentity);
                _context.SaveChanges();
            }

            public void Delete(int id)
            {
                try
                {
                    TEntity tentity = _context.Set<TEntity>().Find(id);
                    _context.Set<TEntity>().Remove(tentity);
                    _context.SaveChanges();
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                
            }

            public void Update(TEntity tentity)
            {
                
                _context.Set<TEntity>().Update(tentity);
                _context.SaveChanges();
            }

        public void save()
        {
            _context.SaveChanges();
        }
        }
    
}