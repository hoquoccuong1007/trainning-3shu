﻿using System;
using EX4.Models;
using EX4.Repository;
using EX4.Services;

namespace EX4.Repository
{
    public interface IClassRepository:IGenericRepository<Class>,IDisposable
    {
       
    }
}