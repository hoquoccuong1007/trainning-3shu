﻿using System;
using EX4.Models;
using EX4.Services;

namespace EX4.Repository
{
    public interface IStudentRepository:IGenericRepository<Student>,IDisposable
    {
        
    }
}