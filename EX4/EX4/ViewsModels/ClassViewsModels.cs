﻿using System.ComponentModel.DataAnnotations;

namespace EX4.ViewsModels
{
    public class ClassViewsModels
    {
        [Key]
        public int IDClass { get; set; }
        public string NameClass { get; set; }
        public string StudyRoom { get; set; }
    }
}