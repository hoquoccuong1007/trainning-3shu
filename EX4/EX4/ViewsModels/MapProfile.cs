﻿using AutoMapper;

namespace EX4.ViewsModels
{
    public class MapProfile: Profile
    {
        public  MapProfile()
        {
            CreateMap<Models.Class,ClassViewsModels>();
            CreateMap<Models.Student,StudentViewsModels>();
        }
    }

   
}