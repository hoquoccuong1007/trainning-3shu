﻿using System;
using System.Collections.Generic;
using EX4.Models;
using EX4.Repository;

namespace EX4.Services
{ 
    public interface IStudentService
    {
        Student GetById(int? Id);
        IEnumerable<Student> GetAll();
        void Insert(Student  student );
        void Delete(int Id);
        void Update(Student student);
        void save();
    }

}