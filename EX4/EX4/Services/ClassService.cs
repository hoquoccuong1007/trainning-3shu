﻿using System.Collections.Generic;
using EX4.Models;
using EX4.UnitOfWork;

namespace EX4.Services
{
    public class ClassService:IClassService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ClassService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Class GetById(int? Id)
        {
            return _unitOfWork.classRepository.GetById(Id);
        }

        public IEnumerable<Class> GetAll()
        {
            return _unitOfWork.classRepository.GetAll();
        }

        public void Insert(Class class1)
        {
            _unitOfWork.classRepository.Insert(class1);
        }

        public void Delete(int Id)
        {
            _unitOfWork.classRepository.Delete(Id);
        }

        public void Update(Class class1)
        {
            _unitOfWork.classRepository.Update(class1);
        }

        public void save()
        {
            _unitOfWork.classRepository.save();
        }
    }
}