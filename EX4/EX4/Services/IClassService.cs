﻿using System.Collections.Generic;
using EX4.Models;

namespace EX4.Services
{
    public interface IClassService
    {
        Class GetById(int? Id);
        IEnumerable<Class> GetAll();
        void Insert(Class class1 );
        void Delete(int Id);
        void Update(Class class1);
        void save();
    }
}