﻿using System;
using System.Collections.Generic;
using AutoMapper;
using EX4.Models;
using EX4.Repository;
using EX4.UnitOfWork;



namespace EX4.Services
{
    public class StudentService : IStudentService
    {
        private readonly IUnitOfWork _unitOfWork;

        public StudentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Student GetById(int? Id)
        {
            return _unitOfWork.studentRepository.GetById(Id);
        }

        public IEnumerable<Student> GetAll()
        {
            return _unitOfWork.studentRepository.GetAll();
        }

        public void Insert(Student student)
        {
            _unitOfWork.studentRepository.Insert(student);
        }

        public void Delete(int Id)
        {
            _unitOfWork.studentRepository.Delete(Id);
        }

        public void Update(Student student)
        {
            _unitOfWork.studentRepository.Update(student);
        }

        public void save()
        {
            _unitOfWork.studentRepository.save();
        }
    }
}