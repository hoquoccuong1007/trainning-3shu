﻿using System.ComponentModel.DataAnnotations;

namespace EX4.Models
{
    public class Class
    {    [Key]
        public int IDClass { get; set; }
        public string NameClass { get; set; }
        public string StudyRoom { get; set; }
    }
}